name = 'Zed A. Shaw'
age = 35 # not a lie
height = 74 # inches
weight = 180
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

puts "Let's talk about %s." % name
puts "He's %d inches (%f centimeters) tall." % [height, height*2.54]
puts "He's %d pounds (%f kilograms) heavy." % [weight, weight*0.45352]
puts "Actually that's not too heavy."
puts "He's got %s eyes and %s hair." % [eyes, hair]
puts "His teeth are usually %s depending on the coffee." %teeth

# this line is tricky, try to get it exactly right
puts "If I add %d, %d, and %d I get %d." % [
    age, height, weight, age + height + weight]
puts "(And if I add %d, %f, and %f I get %f.)" % [
    age, height*2.54, weight*0.45352, age + height*2.54 + weight*0.45352]


